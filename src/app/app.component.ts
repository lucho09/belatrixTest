import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  public loadInformation: boolean = false;

  public listUbigeo =
    {
      'department': [{
        'id': '',
        'name': '',
        'id_father': '',
        'desc_father': ''
      }],
      'province': [{
        'id': '',
        'name': '',
        'id_father': '',
        'desc_father': ''
      }],
      'district': [{
        'id': '',
        'name': '',
        'id_father': '',
        'desc_father': ''
      }]
    };

  private listTextPlane: string = '';

  constructor() { }

  ngOnInit() { }

  fileUpload(event) {
    const reader = new FileReader();
    reader.readAsText(event.srcElement.files[0]);   // source .txt

    reader.onload = () => {
      this.listTextPlane = reader.result.split('\n');

      // declare lists
      let lists: any;
      let listDepartment: any;
      let listProvince: any;
      let listDistrict: any;

      // declare aux names
      let auxNameProvince: string;
      let auxNameDistrict: string;

      for (let i = 0; i < this.listTextPlane.length; i++) {

        lists = this.listTextPlane[i].split('/', 3);
        listDepartment = lists[0].split(" ");
        listProvince = lists[1].split(" ");
        listDistrict = lists[2].split(" ");

        auxNameProvince = '';
        auxNameDistrict = '';

        // validate if province
        if (listProvince.length > 3) {
          auxNameProvince = listProvince[2] + " " + listProvince[3];
        } else {
          auxNameProvince = listProvince[2];
        }

        // validate if district
        if (listDistrict.length > 3) {
          auxNameDistrict = listDistrict[2] + " " + listDistrict[3];
        } else {
          auxNameDistrict = listDistrict[2];
        }

        const auxDepartment = {
          'id': listDepartment[0].substr(1, 2),
          'name': listDepartment[1],
          'id_father': '-',
          'desc_father': '-'
        };

        const auxProvince = {
          'id': listProvince[1],
          'name': (auxNameProvince != undefined) ? auxNameProvince : '',
          'id_father': listDepartment[0].substr(1, 2),
          'desc_father': listDepartment[1]
        };

        const auxDistrict = {
          'id': listDistrict[1],
          'name': (auxNameDistrict != undefined) ? auxNameDistrict : '',
          'id_father': listProvince[1],
          'desc_father': (auxNameProvince != undefined) ? auxNameProvince : ''
        };

        // set array
        this.listUbigeo.department.push(auxDepartment);
        if (auxProvince.name) {
          this.listUbigeo.province.push(auxProvince);
        }
        if (auxDistrict.name) {
          this.listUbigeo.district.push(auxDistrict);
        }
      }

      this.loadInformation = true;
    };
  }

}
